FROM cern/slc6-base
MAINTAINER Ben Jones <ben.dylan.jones@cern.ch>

ADD repos/UMD-4-updates.repo /etc/yum.repos.d/
ADD repos/batch6-stable.repo /etc/yum.repos.d/
ADD repos/wlcg-repository.repo /etc/yum.repos.d/

RUN yum -y update
RUN yum clean all
RUN yum -y groupinstall 'Development Tools'
RUN yum -y install \
  authconfig \
  boost-devel \
  glexec-wn \
  glibc-headers \
  globus-proxy-utils \
  globus-gass-copy-progs \
  lcmaps-plugins-condor-update \
  lcmaps-plugins-mount-under-scratch \
  lcmaps-plugins-namespace \
  redhat-lsb-core \
  sssd \
  sssd-client \
  which

RUN yum -y install \
attr cyrus-sasl-devel e2fsprogs-devel expat-devel singularity \
file-devel giflib-devel gmp-devel gpm-devel kernel-devel libacl-devel \
libattr-devel libcap-devel libcom_err-devel libcurl-devel libdrm-devel \
libdrm-devel libstdc++-devel libuuid-devel libxml2-devel lockdev-devel \
libjpeg-turbo-devel openldap-devel netpbm-devel popt-devel python-devel \
rpm-devel tcl-devel tk-devel openssh-clients PyXML HEP_OSlibs_SL6 \
voms-clients3 wlcg-voms-alice wlcg-voms-atlas wlcg-voms-lhcb wlcg-voms-cms \
wlcg-voms-ops ca-policy-egi-core emi-wn wget

RUN groupadd -r condor
RUN useradd -r -g condor -d /var/lib/condor -s /sbin/nologin condor
RUN mkdir -p /usr/local/condor
RUN mkdir -p /etc/singularity

ADD glexec.conf /etc/
ADD lcmaps-glexec.db /etc/lcmaps/
ADD grid-env-funcs.sh /usr/libexec/
ADD clean-grid-env-funcs.sh /usr/libexec/
ADD grid-vo-env.sh /etc/profile.d/
ADD job_wrapper /usr/local/condor/
ADD singularity.conf /etc/singularity/

RUN chown root:glexec /etc/glexec.conf
RUN chown root:root /etc/lcmaps/lcmaps-glexec.db \
  /usr/libexec/grid-env-funcs.sh \
  /usr/libexec/clean-grid-env-funcs.sh \
  /etc/profile.d/grid-vo-env.sh
RUN chown condor:condor /usr/local/condor/job_wrapper
RUN chmod 640 /etc/glexec.conf /etc/lcmaps/lcmaps-glexec.db
RUN chmod 755 /etc/profile.d/grid-vo-env.sh /usr/local/condor/job_wrapper
RUN chmod 644 /usr/libexec/grid-env-funcs.sh /usr/libexec/clean-grid-env-funcs.sh
